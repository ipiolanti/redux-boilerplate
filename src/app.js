var React = require('react');
var ReactDOM = require('react-dom');
var createStore = require('redux').createStore;
var combineReducers = require('redux').combineReducers;
var Provider = require('react-redux').Provider;
var Router = require('react-router').Router;
var Route = require('react-router').Route;
var createHistory  = require('history').createHistory;
var syncReduxAndRouter = require('redux-simple-router').syncReduxAndRouter;
var routeReducer = require('redux-simple-router').routeReducer;
var Counter = require('../src/components/counter');
//import reducers from '<project-path>/reducers';
require('./styles/main.scss');

//ReactDOM.render(<Counter isActive={false} count={0}/>, document.getElementById('app'));

// TODO: remove this next line. We should import all reducers instead (once we have any)
const reducers = {};

const reducer = combineReducers(Object.assign({}, reducers, {
  routing: routeReducer
}));
const store = createStore(reducer);
const history = createHistory();

// TODO: We should not have this here. It should be on a separate file
const App = React.createClass({
	render() {
		return <Counter isActive={false} count={0}/>;
	}
})

ReactDOM.render(
	<Provider store={store}>
  		<Router history={history}>
    		<Route path="/" component={App} />
    		<Route path="/counter" component={Counter} />
	  	</Router>
  	</Provider>,
	document.getElementById('app')
)
